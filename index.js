"use strict";

var express = require('express'),
	app = express(),
	router = express.Router(),
	hbs = require('express3-handlebars'),
	mongoose = require('mongoose'),
	bodyParser = require('body-parser'),
	multer = require('multer'),
	timestamps = require('mongoose-timestamp');

app.engine('handlebars', hbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

app.use(bodyParser.json());


var PostSchema = new mongoose.Schema({
	title: String,
	content: String
}, {});

PostSchema.plugin(timestamps);

app.locals.template = 'test';

var Posts = mongoose.model('Posts', PostSchema);

mongoose.connect('mongodb://localhost');

app.post('/admin/*',function(req, res, next){
	if (req.body.isAdmin === "true"){
		next();
	} else {
		console.log('fail');
		next(new Error("You aren't an admin"));
	}
})
app.post('/admin/:name', function(req, res, next){
	res.send(200, "You're all good")
})

app.get('/', function(req, res, next){
	var posts = Posts.find({}).exec();

	posts.then(function(posts){
		res.render('index', {
			posts: posts
		})
	}, next);
});

app.get('/posts/:post_id', function(req, res, next){
	var post = Posts.findById(req.param('post_id')).exec();

	post.then(function(post){
		res.render('article',{
			post: post
		})
	}, next);
})

app.post('/posts', function(req, res, next){
	var post = new Posts(req.body);

	post.save(function(err){
		if (err) {next(err); return; }
		res.redirect('/posts/' + post._id);
	})
});

app.use(function(error, req, res, next){
	res.send(500, error.message);
})

app.listen(3000);


module.exports = app;